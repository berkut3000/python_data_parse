# Import hello module

import hello
from os import path
working_dir = path.dirname(__file__)
print(working_dir)

with open(working_dir+'/test.txt') as file:
    file_contents = file.read()
    print(file_contents)
    hello.world()
